const { pgConfig, sshConfig, status } = require('../../config');
const ssh2 = require('ssh2');
const Client = require('pg');
const { proxyServer } = require('../../proxy');

let pgAction = {
  getConnectionString: function () {
    var isLocal = process.env.LOCAL_OR_SSH === "LOCAL" ? true : false;

    return new Promise((resolve, reject) => {
      if (!isLocal) {
        var conn = new ssh2();
        var proxy = proxyServer.createProxyServer(conn);
        conn.connect(sshConfig);
        conn.on('connect', function () {
          status.ready = false;
        });
        conn.on('ready', function () {
          status.ready = true;
          resolve(pgConfig.getProxyConnectionString());
        });
        conn.on('error', function (err) {
          reject(err);
        });
      } else {
        resolve(pgConfig.getConnectionString());
      }
    });
  },
  performQuery: (client, sql) => {
    return new Promise((resolve, reject) => {
      client.connect()
        .catch(function (e) {
          reject(e);
        });
      client.query(sql, (err, res) => {
        if (err) {
          reject(err);
        }
        client.end()
          .then(() => resolve(res))
          .catch((err) => reject(err));
      })
    });
  }
}

module.exports = pgAction;