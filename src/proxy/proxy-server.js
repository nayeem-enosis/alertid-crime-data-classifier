const { pgConfig, status } = require('../config');
const proxy = require('net');

let proxyServer = {
  createProxyServer: function (conn) {
    return proxy.createServer(function(sock) {
      if (!status.ready) {
        return sock.destroy();
      }
      conn.forwardOut(sock.remoteAddress, sock.remotePort, pgConfig.host, pgConfig.port, function(err, stream) {
        if (err) {
          return sock.destroy();
        }
        sock.pipe(stream);
        stream.pipe(sock);
      });
    }).listen(pgConfig.proxyPort, pgConfig.proxyHost);
  }
}

module.exports = proxyServer;