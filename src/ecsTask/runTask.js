const { respond, sqsUtil, rUtil, s3Util, csvUtil, dbUtil } = require('../util');
const { s3Config, sqsConfig, awsConfig, rConfig } = require('../config');

const async = require('async');
const AWS = require('aws-sdk');

const csv = require("fast-csv");

module.exports = {
    main : (rPath = '../R/hello.R') => {
        var messageBody;
        var ReceiptHandle;
        var writeFilePath;
        var fileName;
        var returnData;
        async.waterfall([
            function (next) {
                // read csv file from s3
                s3Util.readDataFromS3( s3Config.classifiedBucketPath + s3Config.fileName , s3Config.classifiedFilePath, next)
            },
            function (next) {
                // read classified data from csv
                csvUtil.readCsvWithHeader(s3Config.classifiedFilePath, (returnData)=>{
                    this.returnData = returnData;
                }, next);
            },
            function (next) {
                // update data into database
                dbUtil.updateAllData(this.returnData, next);
            },
        ], function (err) {
            if (err) {
                console.log('An error has occurred: ' + err);
                process.exit(1);
            }
            else {
                console.log('Successfully processed.');
                process.exit(0);
            }
        });
    }
}