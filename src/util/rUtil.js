var R = require('r-script');
const { exec } = require('child_process');

rUtil = {
    callRScript: function (rFilePath, input, callback, callbackNext) {
        var out = R(rFilePath)
        .data(input)
        .call(function(err, out) {
            if (err) console.log('error from r script: ' + err);
            else {
                callback(out)
            }
            callbackNext(err)
        });
    },
    executeR: function (rFilePath, input, callback, callbackNext) {
        exec(`Rscript ${rFilePath} ${input.join(' ')}`,
        { maxBuffer: Infinity },
        function (err, out) {
            if (err) console.log('error from r script: ' + err);
            else {
                console.log("Classification complete");
            }
            callbackNext(err);
        });
    }
}

module.exports = rUtil;
