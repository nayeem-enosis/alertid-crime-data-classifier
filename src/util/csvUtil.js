const csv = require("fast-csv");

csvUtil = {
    readCsvWithHeader: function (file, returnCsvArray, callbackNext) {
        var returnData = new Array();
        csv
        .fromPath(file, {headers : true})
        .on("data", function(data){
            returnData.push(data);
        })
        .on("error", function(err){
            console.log('error during read csv file');
            callbackNext(err)
        })
        .on("end", function(){
            console.log("Csv file read complete");
            returnCsvArray(returnData);
            callbackNext();
        });
        
    }
}

module.exports = csvUtil;
