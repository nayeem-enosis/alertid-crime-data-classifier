const { pg } = require('../db');
const { Client } = require('pg');


var dbUtil = {
    data: [],
    connectionString: "",
    total: 0,
    initialAll: function (data, connectionString) {
        this.data = data;
        this.connectionString = connectionString;
        this.total = data.length;
    },
    getCrimeTypeUpdateQueryString: function (id, type) {
        return `UPDATE cad 
            SET crime_type= '${type}',
                category_id=(SELECT id FROM categories WHERE name = '${type}')
            WHERE id = ${id};`
    },
    updateDataQuery: function (current) {
        return new Promise((resolve, reject) => {
            var predicted_score = this.data[current]['pred_prob'];
            if (predicted_score >= 0.95) {
                var id = this.data[current]['ID'];
                var crimeType = this.data[current]['predicted_socrata_name'];
                console.log(`Perform query for ${id}`);
                const client = new Client({ connectionString: this.connectionString});
                const queryString = this.getCrimeTypeUpdateQueryString(id, crimeType);
                pg.performQuery(client, queryString)
                    .then((res) => {
                        resolve(current);
                    })
                    .catch((err) => {
                        reject(err)
                    });
            }
            else {
                resolve(current);
            }
        });
    },
    updateAllData: function (data, nextCallback) {
        pg.getConnectionString()
            .then((connectionString) => {
                console.log(connectionString);
                this.initialAll(data, connectionString);
                var loop = (i) => {
                    console.log(i + ' UPDATE')
                    dbUtil.updateDataQuery(i)
                        .then((i) => {
                            i = i+1;
                            if (i< this.total) {
                                loop(i);
                            }
                            else {
                                console.log('All data updated');
                                nextCallback();
                            }
                        })
                        .catch((message) => {
                            console.log(message);
                            nextCallback(message)
                        });
                }
                loop(0);
            })
            .catch((err) => {
                console.log(err);
            });
    }
}

module.exports = dbUtil;
