

const { respond } = require('../util');
const { s3Config, sqsConfig, awsConfig } = require('../config');
const fs = require("fs");

const AWS = require('aws-sdk');
AWS.config.update(
    {
        region: awsConfig.awsRegion
    }
);

var s3 = new AWS.S3();

s3Util = {

    readDataFromS3: function( bucketFilePath, localFilePath, callbackNext) {
        var params = {
            Bucket: 'enosis-dev-crime-classifier',
            Key: bucketFilePath
        };
        s3.getObject(params, function(err, data) {
            if (err) {
                callbackNext(err); // an error occurred
            }
            else {
                fs.writeFileSync(localFilePath, data.Body.toString('ascii'));
                console.log('Csv data read successfully');
                callbackNext();
            }
        });
    },
    uploadDataIntoS3: function (filePath, bucketDirectory, callbackNext) {
        var file = fs.readFileSync(filePath);
        var params = {
            ACL: "public-read-write",
            Body: file,
            Bucket: 'enosis-dev-crime-classifier',
            Key: bucketDirectory
        };
        s3.putObject(params, function (err, data) {
            if (err) console.log(err, err.stack); // an error occurred
            else console.log('Classified data uploaded successfully.');           // successful response
            callbackNext(err)
        });
    },
    deleteUnclassifiedData: function (bucketDirectory, callbackNext) {
        var params = { Bucket: 'enosis-dev-crime-classifier', Key: bucketDirectory };
        s3.deleteObject(params, function (err, data) {
            if (err) console.log(err, err.stack);  // error
            else console.log('Unclassified data deleted'); // deleted
            callbackNext(err)
        });
    }
}

module.exports = s3Util;
