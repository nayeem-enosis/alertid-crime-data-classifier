module.exports = (message, context, callback, statusCode = 200) => {
  var response = {
    statusCode: statusCode,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "Content-Type, api_key, token, username, password",
      "Access-Control-Allow-Methods": "POST, GET, DELETE, OPTIONS",
      "Access-Control-Allow-Credentials": true
    },
    body: JSON.stringify({ "result": message })
  };
  context.callbackWaitsForEmptyEventLoop = false;
  callback(null, response);
}