const { respond } = require('../util');
const { s3Config, sqsConfig, awsConfig } = require('../config');

const AWS = require('aws-sdk');
AWS.config.update(
    {accessKeyId: awsConfig.awsAccessKey, 
    secretAccessKey: awsConfig.awsSecretKey,
    region: awsConfig.awsRegion}
);

var sqs = new AWS.SQS({ apiVersion: '2012-11-05' });

sqsUtil = {
    sendMessage: function (message, nextCallback) {
        var params = {
            MessageAttributes: {
                "Title": {
                    DataType: "String",
                    StringValue: "Unclassified crime data"
                },
                "Author": {
                    DataType: "String",
                    StringValue: "AlertID!"
                }
            },
            MessageBody: message,
            QueueUrl: sqsConfig.queueUrl
        };
        sqs.sendMessage(params, function (err, data) {
            if (err) { 
                console.warn('Error while sending message: ' + err); 
            }
            else { 
                console.info('Message sent, ID: ' + data.MessageId); 
            }
            nextCallback(err);
        });
    },
    receiveLastMessage: function (callback, nextCallback) {

        var params = {
            AttributeNames: [
                "SentTimestamp"
            ],
            MaxNumberOfMessages: 1,
            MessageAttributeNames: [
                "All"
            ],
            QueueUrl: sqsConfig.queueUrl,
            VisibilityTimeout: 0,
            WaitTimeSeconds: 0
        };

        sqs.receiveMessage(params, function (err, data) {
            if (err) {
                console.log("Receive Error", err);
            } else if (data.Messages) {
                messageBody = data.Messages[0].Body;
                callback(messageBody, data.Messages[0].ReceiptHandle)
                console.log("Message received successfully");
            } else {
                err = 'No new message available'
            }
            nextCallback(err);
        });
    },
    deleteMessage: function (ReceiptHandle, nextCallback) {
        var deleteParams = {
            QueueUrl: sqsConfig.queueUrl,
            ReceiptHandle: ReceiptHandle
        };
        sqs.deleteMessage(deleteParams, function (err, data) {
            if (err) {
                console.log("Delete Error", err);
            } else {
                console.log("Message Deleted");
            }
            nextCallback(err);
        });
    }
}

module.exports = sqsUtil;
