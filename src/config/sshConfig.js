
let sshConfig = {
  host : process.env.SSH_HOST || '',
  port : process.env.SSH_PORT || 22,
  username : process.env.SSH_USER ||  '',
  passphrase : process.env.SSH_PASSPHRASE ||  '',
  privateKey : require('fs').readFileSync('./src/config/privatekey'),
};

module.exports = sshConfig;