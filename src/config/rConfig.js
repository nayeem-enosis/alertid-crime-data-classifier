rConfig = {
    writePath : process.env.WRITE_PATH || '',
    workingDirectory : process.env.WORKING_DIRECTORY || '',
    tempInputFilePath : process.env.TEMP_INPUT_FILE_PATH || '',
    tempOutputFilePath : process.env.TEMP_OUTPUT_FILE_PATH || '',
    tempWritePath : process.env.TEMP_WRITE_PATH || ''
}

module.exports = rConfig;
  