awsConfig = {
    awsRegion : process.env.AWS_REGION || 'us-west-2',
    awsAccessKey : process.env.AWS_ACCESS_KEY || '',
    awsSecretKey : process.env.AWS_SECRET_KEY || ''  
}

module.exports = awsConfig;
  