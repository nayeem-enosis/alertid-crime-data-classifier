module.exports = {
    s3Config: require('./s3Config'),
    sqsConfig: require('./sqsConfig'),
    awsConfig: require('./awsConfig'),
    rConfig: require('./rConfig'),
    ecsConfig: require('./ecsConfig'),
    pgConfig: require('./pgConfig'),
    sshConfig: require('./sshConfig'),
    status: require('./status')
}
