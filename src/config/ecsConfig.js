ecsConfig = {
    taskCluster: process.env.AWS_ECS_TASK_CLUSTER || '',
    taskDefinition : process.env.AWS_ECS_TASK_DEFINITION || '',
    taskCount : process.env.AWS_ECS_TASK_COUNT || 1
}

module.exports = ecsConfig;