
let pgConfig = {
  username: process.env.PG_DB_USER || '',
  password: process.env.PG_DB_PASS || '',
  host: process.env.PG_HOST || '',
  database: process.env.PG_DB || '',
  port: process.env.PG_PORT || 5432,
  getConnectionString: function () {
    return 'postgresql://' + this.username + ':' + this.password + '@' + this.host + 
          ':' + this.port + '/' + this.database;
  },
  
  proxyHost: '',
  proxyPort: 9091,
  getProxyConnectionString: function () {
    return 'postgresql://' + this.username + ':' + this.password + '@' + this.proxyHost + 
          ':' + this.proxyPort + '/' + this.database;
  }
};

module.exports = pgConfig;