
s3Config = {
    filePrefix  : process.env.FILE_PREFIX || '',  
    fileSuffix  : process.env.FILE_SUFFIX || '',
    classifiedFilePath : process.env.CLASSIFIED_FILE_PATH || 'src/R/prediction_output/export.csv',
    classifiedBucketPath : process.env.BUCKET_PATH || 'classified_crime_data/',  
    fileName    : process.env.FILE_NAME || 'export.csv'
}

module.exports = s3Config;
  