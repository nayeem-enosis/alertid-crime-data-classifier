const { respond, sqsUtil } = require('../util');
const { s3Config, sqsConfig, awsConfig, ecsConfig } = require('../config');

const async = require('async');
const aws = require('aws-sdk');

exports.handler = (event, context, callback) => {


  var re = new RegExp('^' + s3Config.filePrefix.replace('/', '\/') + '.+\.' + s3Config.fileSuffix + '$');

  if (!event.Records[0].s3.object.key) {
    respond('uploaded file not found', context, callback, 422);
  }
  else {
    var key = event.Records[0].s3.object.key;
    if (!re.exec(key)) {
      respond('csv file is not valid', context, callback, 422);
    }
    else {
      // send a sqs message
      var sqs = new aws.SQS({ apiVersion: '2012-11-05' });
      var ecs = new aws.ECS({apiVersion: '2014-11-13'});
      async.waterfall([

        function (next) {
          sqsUtil.sendMessage(key, next);
        },
        function (next) {
          // Starts an ECS task to work through the feeds.
          var params = {
            cluster: ecsConfig.taskCluster,
            taskDefinition: ecsConfig.taskDefinition,
            count: ecsConfig.taskCount
          };
          ecs.runTask(params, function (err, data) {
            if (err) { console.warn('error: ', "Error while starting task: " + err); }
            else { console.info('Task ' + ecsConfig.taskDefinition + ' started: ' + JSON.stringify(data.tasks)) }
            next(err);
          });
        }
      ], function (err) {
        if (err) {
          respond('An error has occurred: ' + err, context, callback, 400);
        }
        else {
          respond('Successfully processed.', context, callback, 400);
        }
      });
    }
  }
};
