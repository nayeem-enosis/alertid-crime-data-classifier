FROM ubuntu:14.04

# install our dependencies and nodejs
RUN apt-get update
RUN curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
RUN apt-get install -y nodejs

RUN mkdir -p /usr/alertid-crime-data-classifier

WORKDIR /usr/alertid-crime-data-classifier/

# copy file in working directory
COPY package.json /usr/alertid-crime-data-classifier
RUN npm install && npm cache clean --force
COPY . /usr/alertid-crime-data-classifier

WORKDIR /usr/alertid-crime-data-classifier/

CMD [ "node", "index.js"]